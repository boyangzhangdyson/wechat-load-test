from datetime import datetime
from threading import Thread, Event


class Task(Thread):
    def __init__(self, task, interval, life_time):
        Thread.__init__(self)
        self.task = task
        self.interval = interval
        self.life_time = life_time
        self._is_stopped = Event()

    def run(self):
        start_time = datetime.now()

        while (datetime.now() - start_time).total_seconds() < self.life_time and not self._is_stopped:
            self.task()
            self._is_stopped.wait(self.interval)

    def stop(self):
        self._is_stopped = True
