# a mapping from function names and its number of parameters
functions = {
    # base functions
    "say_hello": 1,
    "say_goodbye": 1,
    "say_im_back": 0,
    "report_error": 1,
    "report_invalid_input": 0,
    "log_data": 2,
    "report_upgrading_software": 1,
    "wait_for_command": 2,
    # product_520 functions
    "report_env_data": 0
}


def validate_user_input(args):
    input_scenario = args["scenario"]

    if not input_scenario:
        raise ValueError('empty scenario')

    input_steps = input_scenario["steps"]

    if not input_steps:
        raise ValueError('empty steps')

    # steps are in a list
    for step in input_steps:
        function_name = step.split('(')[0]
        if functions[function_name] is None:
            raise ValueError('this step does not exist, please enter an existing step')

        parameters = step.split('(')[1].split(')')[0]
        num_of_parameters = len(parameters.split(','))

        if num_of_parameters == 1:
            first_para = parameters.split(',')[0]
            if not first_para:
                num_of_parameters = 0

        if num_of_parameters > functions[function_name]:
            raise ValueError('the number of parameters in step: ' + function_name + ' is invalid')

    return True
