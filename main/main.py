import argparse
import yaml
import random
import sys
from products.base import Base
from products.product_520 import Product_520
from main.user_input_parser.user_input_validation import validate_user_input


"""
    Return product life time ranging from 1 hour to 10 hours in seconds 
"""
def set_life_time():
    return random.randint(1, 10) * 3600


def execute(args):
    if args.type == 520 or args.type == 527 or args.type == 438:
        product = Product_520(args.type, args.serial, args.env, args.password, args.broker, cert_path=args.cert)
    else:
        product = Base(args.type, args.serial, args.env, args.password, args.broker, cert_path=args.cert)

    if args.provision is True:
        product.provision_product(args.email, args.credentials)

    product.start("ENV-DATA", 20 * 60, set_life_time())

    execute_scenarios(args)

    product.stop()


def execute_scenarios(args):
    if validate_user_input(args):
        eval(str(args["scenario"]["steps"]))


def validate_args(args):
    if not args.serial:
        print(f"Please enter the serial number of the product that needs to be emulated")
        sys.exit(-1)

    if not args.type:
        print(f"Please enter the product type of the product that needs to be emulated")
        sys.exit(-1)

    if not args.env:
        print(f"Please enter the environment where the product needs to be emulated")
        sys.exit(-1)

    if args.provision is True and (not args.cert or not args.email or not args.credentials):
        print(f"Please specify required params (email, password, certs), needed to provision")
        sys.exit(-1)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Device emulation tool required to test cloud services')

    """ Using YAML doc to specify arguments (take precedence over the other arguments) """
    parser.add_argument("-y", "--yaml", help="path of the yaml file")

    """ Mandatory arguments """
    parser.add_argument("-s", "--serial", help="serial number of the product")
    parser.add_argument("-e", "--env", help="environment the device is on")
    parser.add_argument("-t", "--type", help="type of the device")

    """ Optional arguments"""
    parser.add_argument("-b", "--broker", help="broker to connect to")
    parser.add_argument("-p", "--password", help="password used to connect to the broker")

    """ Provisioning arguments """
    parser.add_argument("-P", "--provision", action="store_true", help="provision a device")
    parser.add_argument("-E", "--email", help="Email used to sync")
    parser.add_argument("-C", "--credentials", help="Password of the app")
    parser.add_argument("-c", "--cert", help="client certs used for auth")

    args = parser.parse_args()

    if args.yaml is not None:
        try:
            file = open(args.yaml, 'r')
            args = yaml.load(file)
        except FileNotFoundError:
            print("File is not found")

    validate_args(args)
    execute(args)