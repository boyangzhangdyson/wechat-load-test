import paho.mqtt.client as mqtt
import json
import random, string
import logging
import gevent

config = json.load(open('../config-lt.json'))

serverCert = config['SERVER_CERT_PATH']

logging.basicConfig(filename="./mqtt-receiver.log",
                    level=logging.INFO,
                    format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
                    datefmt='%m-%d %H:%M:%S.%f',
                    filemode='w')


def id_generator(size=6, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))


def connect(host, port=443):
    mqttc = mqtt.Client(client_id=id_generator(size=12), protocol=mqtt.MQTTv31)

    mqttc.tls_set(serverCert)
    mqttc.tls_insecure_set(True)
    mqttc.username_pw_set('DysonCloudServices', config['MQTT_PASSWORD'])
    mqttc.user_data_set({"broker": host})
    mqttc.on_message = on_message
    mqttc.on_connect = on_connect
    mqttc.connect(host, port)
    mqttc.loop_start()

    mqttc.subscribe(topic='520/+/command')
    mqttc.subscribe(topic='520/+/status/connection')


def on_connect(self, userdata, flags_dict, result):
    logging.info(f"connect:{userdata['broker']} = {result}")


def on_message(self, userdata, message):
    logging.info(f"{userdata['broker']}:{message.topic}:{message.payload}")


connect('broker1.cplt.dyson.com')
connect('broker2.cplt.dyson.com')
connect('broker3.cplt.dyson.com')
connect('broker4.cplt.dyson.com')
connect('broker5.cplt.dyson.com')
connect('broker6.cplt.dyson.com')
connect('broker7.cplt.dyson.com')
connect('broker8.cplt.dyson.com')


while True:
    gevent.sleep(1000)