from redis import ConnectionPool, Redis
import json, datetime, urllib3, os
import boto3
from locust import TaskSet, Locust, task, events
from locust.exception import StopLocust
import mqtt.client as mqtt
from threading import Condition
import random, time
import cbor2
import gevent
import base64
import requests
from requests.auth import HTTPBasicAuth

redis_server = os.environ['MASTER_IP'] if 'MASTER_IP' in os.environ else 'localhost'

urllib3.disable_warnings(urllib3.exceptions.SecurityWarning)
redis_conn_pool = ConnectionPool(host=redis_server, port=6379)
Redis(connection_pool=redis_conn_pool).set("ota:loadtest-deviceid:product", 0)

config = json.load(open('config-lt.json'))
session = boto3.session.Session(
    aws_access_key_id=config['AWS_ACCESS_KEY_ID'],
    aws_secret_access_key=config['AWS_SECRET_ACCESS_KEY'],
    region_name='eu-west-1')

dynamodb = session.resource('dynamodb')
serverCert = config['SERVER_CERT_PATH']

MQTT_NOT_CONNECTED = 100


class ConnectError(Exception):
    def __init__(self, message):
        super(ConnectError, self).__init__(message)


class ResponseError(Exception):
    def __init__(self, message):
        super(ResponseError, self).__init__(message)


def fire_failure(request_type, name, response_time, exception, **kwargs):
    events.request_failure.fire(
        request_type=request_type, name=name, response_time=response_time, exception=exception, **kwargs)


def fire_success(request_type, name, response_time=0, response_length=0, **kwargs):
    events.request_success.fire(
        request_type=request_type, name=name, response_time=response_time, response_length=response_length, **kwargs)


def time_delta(t1, t2):
    return int((t2 - t1) * 1000)


def gen_message():
    data = {}
    cfg = {
        'aqlm': {'min': 0, 'max': 990},
        'volm': {'min': 0, 'max': 990},
        'no2m': {'min': 0, 'max': 990},
        'p25m': {'min': 0, 'max': 9999},
        'p10m': {'min': 0, 'max': 9999},
        'tmpm': {'min': 2430, 'max': 3530},
        'fnsp': {'min': 0, 'max': 100},
        'humm': {'min': 0, 'max': 990},
        'fnmd': {'min': 1, 'max': 5},
        'fnau': {'min': 0, 'max': 60},
        'fnon': {'min': 0, 'max': 60}
    }
    maxBuckets = 20

    for k, v in cfg.items():
        bucket = [0] * maxBuckets
        for i in range(maxBuckets):
            value = random.randint(v['min'], v['max'])
            if value % 50 == 0: value = -1
            bucket[i] = value
        data[k] = bucket

    data["tmsp"] = int(time.time())
    data["msg"] = "ENV-DATA"

    return base64.b64encode(cbor2.dumps(data, datetime_as_timestamp=True, timezone=cbor2.compat.timezone.utc)).decode('utf-8')


class ProductBehaviour(TaskSet):

    def wait_for_notify(self, timeout=30):
        self.condition.acquire()
        result = self.condition.wait(timeout)
        self.condition.release()
        return result

    def notify(self):
        self.condition.acquire()
        self.condition.notify()
        self.condition.release()

    def on_connect(self, client, userdata, flags_dict, result):
        self.connection_status = result
        self.notify()

    def on_message(self, client, userdata, message):
        if b"SOFTWARE-UPGRADE" in message.payload:
            self.notify()

    def say_hello(self, version='1.0.0.0'):
        msg = {
            "msg": "HELLO",
            "time": datetime.date.today().isoformat(),
            "model": self.productType,
            "version": version,
            "protocol": "1.0.0"
        }
        payload = json.dumps(msg)

        mmi = self.mqttc.publish(topic=self.hello_topic, payload=payload)
        mmi.wait_for_publish()

        fire_success('MQTT', f"publish:HELLO:{version}")

    def init(self):
        idx = Redis(connection_pool=redis_conn_pool).incr("ota:loadtest-deviceid:product")
        record = dynamodb.Table('LoadTest_Data').get_item(Key={'id': idx})

        # if 'Item' not in record:
        #     raise Exception('No more agents required')

        self.idx = idx

        self.data = json.loads(record['Item']['data'])
        self.username = self.data['productSerial']
        self.password = self.data['tokenPassword']
        self.host = self.data['broker']
        self.serialNumber = self.data['productSerial']
        self.productType = self.data['product']
        self.topicBase = f"{self.productType}/{self.serialNumber}"
        self.ca_cert = config['SERVER_CERT_PATH']
        self.hello_topic = f"{self.topicBase}/status/connection"
        self.env_topic = f"{self.topicBase}/status/summary"
        self.upgrade_topic = f"{self.topicBase}/command"
        self.client_id = self.data['productSerial']

        self.condition = Condition()

    def connect(self):
        self.mqttc = mqtt.Client(client_id=self.data['productSerial'], protocol=mqtt.MQTTv31)

        self.mqttc.tls_set(self.ca_cert)
        self.mqttc.tls_insecure_set(True)
        self.mqttc.username_pw_set(self.username, self.password)

        self.mqttc.on_connect = self.on_connect

        self.connection_status = MQTT_NOT_CONNECTED

        [host, port] = self.host.split(":")
        result = self.mqttc.connect(host, int(port))

        self.mqttc.loop_start()
        self.loop_started = True

        self.wait_for_notify()

        return self.connection_status == mqtt.MQTT_ERR_SUCCESS

    def end(self):
        if self.loop_started: self.mqttc.loop_stop()
        if self.connection_status == mqtt.MQTT_ERR_SUCCESS: self.mqttc.disconnect()

        raise StopLocust()

    def log(self, msg=''):
        print(f'{self.idx}:{self.serialNumber}: {msg}')

    def on_start(self):
        fire_success('Client', 'Product')
        self.init()
        self.log('created')

        retry_count = 5
        while True:
            if not self.connect():
                retry_count -= 1
                if retry_count > 0:
                    gevent.sleep(1)
                    continue
                break
            break

        if self.connection_status == mqtt.MQTT_ERR_SUCCESS:
            fire_success('MQTT', 'connect')
            self.log('connected')
        else:
            fire_failure('MQTT', 'connect', 0, ConnectError(f'Error:{self.connection_status}'))
            self.log(f'ConnectError:{self.connection_status}')
            self.end()

        self.say_hello()

    @task
    def send_env_data(self):
        msg = {
            "time": datetime.datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%S%z') + 'Z',
            "bin": gen_message()
        }
        self.log('ENV-DATA sent')

        mmi = self.mqttc.publish(topic=self.env_topic, payload=json.dumps(msg))
        mmi.wait_for_publish()
        fire_success('MQTT', f"publish:ENV-DATA")
        #gevent.sleep(9)


class AppBehaviour(TaskSet):

    def log(self, msg=''):
        print(f'{msg}')

    def load_next_device(self):
        self.idx = Redis(connection_pool=redis_conn_pool).incr("ota:loadtest-deviceid:product")
        record = dynamodb.Table('LoadTest_Data').get_item(Key={'id': self.idx})
        self.data = json.loads(record['Item']['data'])

    def on_start(self):
        fire_success('Client', 'App')
        self.load_next_device()

    def call_api(self, endpoint):
        api = f"environmentdata/{endpoint}"
        resp = requests.get(
            f"https://api.cplt.dyson.com/v1/messageprocessor/devices/{self.data['productSerial']}/{api}",
            auth=HTTPBasicAuth(self.data['accountGuid'], self.data['accountPassword']),
            verify=serverCert)
        if resp.status_code == 200:
            fire_success('HTTP', api)
        elif resp.status_code == 401:
            self.log(f"{self.data['productSerial']} login error")
            self.load_next_device()
        else:
            fire_failure('HTTP', api, 0, ResponseError(f'Error:{resp.status_code}'))

    @task
    def get_weekly(self):
        self.call_api('weekly')

    @task
    def get_daily(self):
        self.call_api('daily')


class ProductLocust(Locust):
    weight = 45000
    task_set = ProductBehaviour
    min_wait = 20 * 60 * 1000
    max_wait = 20 * 60 * 1000


class AppLocust(Locust):
    weight = 500
    task_set = AppBehaviour
    min_wait = 30 * 60 * 1000
    max_wait = 30 * 60 * 1000
