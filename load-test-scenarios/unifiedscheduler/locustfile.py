from redis import ConnectionPool, Redis
import json, urllib3, os
import boto3
from locust import TaskSet, Locust, task, events
import time
import gevent

import requests
from requests.auth import HTTPBasicAuth

redis_server = os.environ['MASTER_IP'] if 'MASTER_IP' in os.environ else 'localhost'

urllib3.disable_warnings(urllib3.exceptions.SecurityWarning)
redis_conn_pool = ConnectionPool(host=redis_server, port=6379)
Redis(connection_pool=redis_conn_pool).set("ota:loadtest-deviceid:product", 0)

config = json.load(open('config-lt.json'))
session = boto3.session.Session(
    aws_access_key_id=config['AWS_ACCESS_KEY_ID'],
    aws_secret_access_key=config['AWS_SECRET_ACCESS_KEY'],
    region_name='eu-west-1')

dynamodb = session.resource('dynamodb')
serverCert = config['SERVER_CERT_PATH']

MQTT_NOT_CONNECTED = 100


class ConnectError(Exception):
    def __init__(self, message):
        super(ConnectError, self).__init__(message)


class ResponseError(Exception):
    def __init__(self, message):
        super(ResponseError, self).__init__(message)


def fire_failure(request_type, name, response_time, exception, **kwargs):
    events.request_failure.fire(
        request_type=request_type, name=name, response_time=response_time, exception=exception, **kwargs)


def fire_success(request_type, name, response_time=0, response_length=0, **kwargs):
    events.request_success.fire(
        request_type=request_type, name=name, response_time=response_time, response_length=response_length, **kwargs)


def time_delta(t1, t2):
    return int((t2 - t1) * 1000)

def gen_request():
    return {
    'enabled': 'true',
    'events': [
        {
            'groupId': 1,
            'weeklyRepeat': 'false',
            'days': [
                0,
                1,
                2,
                3,
                4,
                5,
                6
            ],
            'enabled': 'true',
            'startTime': '09:00:00',
            'settings': {
                'fdir': 'ON',
                'fnsp': '9',
                'oson': 'OFF',
                'nmod': 'OFF',
                'auto': 'OFF'
            }
        },
        {
            'groupId': 1,
            'weeklyRepeat': 'false',
            'days': [
                0,
                1,
                2,
                3,
                4,
                5,
                6
            ],
            'enabled': 'true',
            'startTime': '17:00:00',
            'settings': {
                'fdir': 'OFF',
                'fnsp': '9',
                'oson': 'OFF',
                'nmod': 'OFF',
                'auto': 'OFF'
            }
        },
        {
            'groupId': 2,
            'weeklyRepeat': 'false',
            'days': [
                0,
                1,
                2,
                3,
                4,
                5,
                6
            ],
            'enabled': 'true',
            'startTime': '18:00:00',
            'settings': {
                'fdir': 'ON',
                'fnsp': '6',
                'oson': 'OFF',
                'nmod': 'OFF',
                'auto': 'OFF'
            }
        },
        {
            'groupId': 2,
            'weeklyRepeat': 'false',
            'days': [
                0,
                1,
                2,
                3,
                4,
                5,
                6
            ],
            'enabled': 'true',
            'startTime': '20:00:00',
            'settings': {
                'fdir': 'OFF',
                'fnsp': '6',
                'oson': 'OFF',
                'nmod': 'OFF',
                'auto': 'OFF'
            }
        },
        {
            'groupId': 3,
            'weeklyRepeat': 'false',
            'days': [
                0,
                1,
                2,
                3,
                4,
                5,
                6
            ],
            'enabled': 'true',
            'startTime': '21:00:00',
            'settings': {
                'fdir': 'ON',
                'fnsp': '3',
                'oson': 'OFF',
                'nmod': 'OFF',
                'auto': 'OFF'
            }
        },
        {
            'groupId': 3,
            'weeklyRepeat': 'false',
            'days': [
                0,
                1,
                2,
                3,
                4,
                5,
                6
            ],
            'enabled': 'true',
            'startTime': '23:00:00',
            'settings': {
                'fdir': 'OFF',
                'fnsp': '3',
                'oson': 'OFF',
                'nmod': 'OFF',
                'auto': 'OFF'
            }
        }
    ]
}


class AppPutBehaviour(TaskSet):
    def log(self, msg=''):
        print(f'{msg}')

    def load_next_device(self):
        self.idx = Redis(connection_pool=redis_conn_pool).incr("ota:loadtest-deviceid:product")
        record = dynamodb.Table('LoadTest_Data').get_item(Key={'id': self.idx})
        self.data = json.loads(record['Item']['data'])

    def on_start(self):
        fire_success('Client', 'AppPut')
        self.load_next_device()

    def put_request(self, serial):
        url = f"https://api.cplt.dyson.com/v1/unifiedscheduler/{serial}/events?productType=N520"
        report = f"Put:UnifiedScheduler"
        self.log(f"PUT request for {url}")
        now = time.time()
        resp = requests.put(
            url,
            json.dumps(gen_request()),
            auth=HTTPBasicAuth(self.data['accountGuid'], self.data['accountPassword']),
            headers={'Content-Type': 'application/json'},
            verify=serverCert)

        if resp.status_code == 200:
            fire_success('HTTP', report, time_delta(now, time.time()))
        elif resp.status_code == 401:
            self.log(f"{self.data['productSerial']} login error. Error:{resp.status_code}")
            self.load_next_device()
            self.put_request(self.data['productSerial'])
        else:
            fire_failure('HTTP', report, 0, ResponseError(f'Error:{resp.status_code}'))

    @task
    def schedule(self):
        self.put_request(self.data['productSerial'])


class AppGetBehaviour(TaskSet):

    def log(self, msg=''):
        print(f'{msg}')

    def load_next_device(self):
        self.idx = Redis(connection_pool=redis_conn_pool).incr("ota:loadtest-deviceid:product")
        record = dynamodb.Table('LoadTest_Data').get_item(Key={'id': self.idx})
        self.data = json.loads(record['Item']['data'])

    def on_start(self):
        fire_success('Client', 'AppGet')
        self.load_next_device()

    def get_request(self, serial):
        url = f"https://api.cplt.dyson.com/v1/unifiedscheduler/{serial}/events?productType=N520"
        report = f"Get:UnifiedScheduler"
        now = time.time()
        self.log(f"GET request for {url}")
        resp = requests.get(
            url,
            auth=HTTPBasicAuth(self.data['accountGuid'], self.data['accountPassword']),
            verify=serverCert)
        if resp.status_code == 200:
            fire_success('HTTP', report, time_delta(now, time.time()))
        elif resp.status_code == 401:
            self.log(f"{self.data['productSerial']} login error")
            self.load_next_device()
        else:
            fire_failure('HTTP', report, 0, ResponseError(f'Error:{resp.status_code}'))

    @task
    def schedule(self):
        self.get_request(self.data['productSerial'])


class ProductBehaviour(TaskSet):
    def log(self, msg=''):
        print(f'{msg}')

    def load_next_device(self):
        self.idx = Redis(connection_pool=redis_conn_pool).incr("ota:loadtest-deviceid:product")
        record = dynamodb.Table('LoadTest_Data').get_item(Key={'id': self.idx})
        self.data = json.loads(record['Item']['data'])

    def on_start(self):
        fire_success('Client', 'Product')
        self.load_next_device()

    def get_bin(self, serial):
        url = f"https://api.cplt.dyson.com/v1/unifiedscheduler/{serial}/schedule.bin"
        report = f"Get:ScheduleBin"
        self.log(f"GET Bin for {url}")
        now = time.time()
        resp = requests.get(
            url,
            auth=HTTPBasicAuth(self.data['accountGuid'], self.data['accountPassword']),
            verify=serverCert)
        if resp.status_code == 200:
            fire_success('HTTP', report, time_delta(now, time.time()))
        elif resp.status_code == 401:
            self.log(f"{self.data['productSerial']} login error")
            self.load_next_device()
        else:
            fire_failure('HTTP', report, 0, ResponseError(f'Error:{resp.status_code}'))
    @task
    def schedule(self):
        self.get_bin(self.data['productSerial'])


class AppPutLocust(Locust):
    weight = 1
    task_set = AppPutBehaviour
    min_wait = 50 * 1000
    max_wait = 50 * 1000


class AppGetLocust(Locust):
    weight = 1
    task_set = AppGetBehaviour
    min_wait = 3 * 1000
    max_wait = 3 * 1000


class ProductLocust(Locust):
    weight = 6
    task_set = ProductBehaviour
    min_wait = 1 * 1000
    max_wait = 1 * 1000

