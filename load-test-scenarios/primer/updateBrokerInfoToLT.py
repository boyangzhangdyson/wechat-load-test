
import boto3
import json
import logging, os
import threading
import urllib3
urllib3.disable_warnings(urllib3.exceptions.SecurityWarning)

logfile = "./app.log"
os.remove(logfile)

logging.getLogger("botocore").setLevel(logging.ERROR)
logging.basicConfig(filename=logfile, level=logging.INFO, filemode='w')

config = json.load(open('../config-lt.json'))

session = boto3.session.Session(aws_access_key_id=config['AWS_ACCESS_KEY_ID'], aws_secret_access_key=config['AWS_SECRET_ACCESS_KEY'])

dynamodb = session.resource('dynamodb')

total_items = []
total_scanned_count = 0


def get_item(idx):
    record = dynamodb.Table('LoadTest_Data').get_item(Key={'id': idx})

    data = json.loads(record['Item']['data'])
    device = dynamodb.Table('Devices').get_item(Key={'Serial': data['productSerial']})

    return data, json.loads(device['Item']['Data'])


class Runner(threading.Thread):
    def __init__(self, _id, items):
        threading.Thread.__init__(self)
        self.id = _id
        self.items = items

    def run(self):
        for idx in self.items:
            data, device = get_item(idx)
            data['broker'] = device['ScaleUnit'].replace("SU0", "broker") + ".cplt.dyson.com:443"

            dynamodb.Table('LoadTest_Data').update_item(
                Key={
                    'id': idx
                },
                ExpressionAttributeNames={
                    '#data': 'data',
                },
                UpdateExpression='SET #data = :val',
                ExpressionAttributeValues={
                    ':val': json.dumps(data)
                }
            )

            logging.info(f"{self.id}:{idx}|{data['productSerial']}|Done")


for x in range(45000):
    total_items.append(x+1)

num_threads = 400
items_per_thread = round(len(total_items) / num_threads)

chunks = [total_items[x:x+items_per_thread] for x in range(0, len(total_items), items_per_thread)]

threads = []
tid = 1
for chunk in chunks:
    t = Runner(tid, chunk)
    tid += 1
    t.start()
    threads.append(t)

for t in threads:
    t.join()
