from redis import ConnectionPool, Redis
import requests, json, time
import boto3, botocore
import paho.mqtt.client as mqtt
import paho.mqtt.publish as publish
import gevent

from locust import TaskSet, task, Locust, events
from locust.exception import StopLocust

import logging
logging.getLogger("requests").setLevel(logging.WARNING)
logging.basicConfig(filename="./app.log", level=logging.INFO)

import urllib3
urllib3.disable_warnings(urllib3.exceptions.SecurityWarning)

config = json.load(open('config-lt.json'))
session = boto3.session.Session(
    aws_access_key_id=config['AWS_ACCESS_KEY_ID'],
    aws_secret_access_key=config['AWS_SECRET_ACCESS_KEY'])

dynamodb = session.resource(
    'dynamodb',
    config=botocore.config.Config(max_pool_connections=1000))

environmentUrl = f"https://api{config['ENV_PREFIX']}.dyson.com/"
userRegistrationUrl = 'v2/userregistration/register'
userAuthenticateUrl = 'v1/userregistration/authenticate'
provisioningUrl = 'v1/provisioningservice/deviceprovisioning/'
messageProcessorUrl = 'v1/messageprocessor/'
serverCert = config['SERVER_CERT_PATH']

REDIS_HOST = "localhost"
REDIS_PORT = 6379
REDIS_KEY = "ota:loadtest:deviceid"

redis_conn_pool = ConnectionPool(host=REDIS_HOST, port=REDIS_PORT)
Redis(connection_pool=redis_conn_pool).set(REDIS_KEY, 0)


def mqtt_publish(host, port, topic, payload, username, password):
    auth = {"username": username, "password": password}
    ssl_opts = {"ca_certs": serverCert}
    publish.single(topic, protocol=mqtt.MQTTv31, payload=payload, hostname=host, port=port, auth=auth, tls=ssl_opts)


def fire_success(name, ms = 0):
    events.request_success.fire(
        request_type='OP',
        name=name,
        response_time=ms,
        response_length=0
    )


class Behaviour(TaskSet):

    def init(self):
        self.start = time.time()
        self.init_time = self.start

        idx = Redis(connection_pool=redis_conn_pool).incr(REDIS_KEY)
        record = dynamodb.Table('LoadTest_Data').get_item(Key={'id': idx})
        if 'Item' not in record:
            logging.error(f"{idx}|StopLocust")
            raise StopLocust()

        self.data = json.loads(record['Item']['data'])
        self.idx = idx

    def fire_failure(self, name, resp=None, ms=0):

        logging.error(f"{self.data['productSerial']}|{name}|{resp.status_code}|{resp.text}")
        events.request_failure.fire(
            request_type='OP',
            name=name,
            response_time=ms,
            exception='Error-' + str(resp.status_code)
        )

    def elapsed(self):
        now = time.time()
        result = (now - self.start) * 100
        self.start = now
        return round(result)

    def register(self):

        retry_count = 5
        while True:
            resp = requests.post(
                f"{environmentUrl}{userRegistrationUrl}?culture={self.data['culture']}&country={self.data['country']}",
                data=self.data,
                verify=serverCert)

            if resp.status_code == 400 and 'EMAIL_ALREADY_REGISTERED' in resp.text:
                pass
            elif resp.status_code != 200:

                retry_count -= 1
                if retry_count > 0:
                    gevent.sleep(1)
                    continue

                self.fire_failure(name='register-error', resp=resp)
                return False

            fire_success('registered', self.elapsed())
            break

        return True

    def login(self):

        retry_count = 5
        while True:
            resp = requests.post(f"{environmentUrl}{userAuthenticateUrl}?country={self.data['country']}",
                                 data={"Email": self.data['email'], "Password": self.data['password']}, verify=serverCert)

            response_time = self.elapsed()
            if resp.status_code != 200:

                retry_count -= 1
                if retry_count > 0:
                    gevent.sleep(1)
                    continue

                self.fire_failure(name='login-error', resp=resp, ms=response_time)
                return False

            fire_success('loggedIn', response_time)

            self.auth_header = resp.headers['Authorization']
            self.user = json.loads(resp.text)
            break

        return True

    def provision_device(self):

        retry_count = 5
        while True:

            resp = requests.post(f"{environmentUrl}{provisioningUrl}{self.data['productSerial']}",
                                 cert=config['CLIENT_CERT_PATH'], verify=serverCert)
            response_time = self.elapsed()

            if resp.status_code != 200 and resp.status_code != 201:
                retry_count -= 1
                if retry_count > 0:
                    gevent.sleep(1)
                    continue

                self.fire_failure(name='prov-error', resp=resp, ms=response_time)
                return False

            self.prov_data = json.loads(resp.text)
            fire_success('provisioned', response_time)
            self.update_table()
            break

        return True

    def update_table(self):

        self.data['broker'] = f'{self.prov_data["BrokerHostName"]}:{self.prov_data["BrokerPort"]}'
        self.data['tokenPassword'] = self.prov_data["TokenPassword"]

        dynamodb.Table('LoadTest_Data').update_item(
            Key={
                'id': self.idx
            },
            ExpressionAttributeNames={
                '#data': 'data',
            },
            UpdateExpression='SET #data = :val',
            ExpressionAttributeValues={
                ':val': json.dumps(self.data)
            }
        )

    def send_avu(self):

        avu_msg = {
            "msg": "AUTHORISE-VERIFIED-USER",
            "time": "2015-04-29T14:30:00Z",
            "id": self.user['Account'],
            "localpwd": self.user['Password']
        }

        mqtt_publish(
            self.prov_data["BrokerHostName"],
            self.prov_data["BrokerPort"],
            f"{self.data['product']}/{self.data['productSerial']}/status/auth",
            json.dumps(avu_msg),
            self.prov_data["TokenId"],
            self.prov_data["TokenPassword"])

    def check_connection_status(self):

        headers = {"Authorization": self.auth_header}

        retry_count = 10
        while True:
            resp = requests.get(f"{environmentUrl}{messageProcessorUrl}devices/{self.data['productSerial']}/connectionstatus",
                                verify=serverCert, headers=headers)
            response_time = round((time.time() - self.init_time) * 100)
            if resp.status_code != 200:
                retry_count -= 1
                if retry_count > 0:
                    gevent.sleep(1)
                    continue

                self.fire_failure(name='connection-error', resp=resp, ms=response_time)
                return False

            fire_success('connected-200', ms=response_time)
            break

        return True

    @task
    def task_to_run(self):

        self.init()

        fire_success('started')

        # if self.register() is False: return

        # gevent.sleep(1)

        if self.login() is False: return

        # gevent.sleep(1)

        # if self.provision_device() is False: return

        # gevent.sleep(2)

        # self.send_avu()

        # gevent.sleep(2)

        # self.check_connection_status()


class Agent(Locust):
    task_set = Behaviour
