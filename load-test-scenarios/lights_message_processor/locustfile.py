import boto3
from locust import TaskSet, Locust, task, events
from locust.exception import StopLocust
from redis import ConnectionPool, Redis
import json

config = json.load(open('./config-lt.json'))

session = boto3.session.Session(
    aws_access_key_id=config['AWS_ACCESS_KEY_ID'],
    aws_secret_access_key=config['AWS_SECRET_ACCESS_KEY'],
    region_name='eu-west-1')

dynamodb = session.resource('dynamodb')

snsClient = boto3.client(
    'sns',
    aws_access_key_id=config['AWS_ACCESS_KEY_ID'],
    aws_secret_access_key=config['AWS_SECRET_ACCESS_KEY'],
    region_name='us-east-2'
)

REDIS_HOST = "localhost"
REDIS_PORT = 6379
REDIS_KEY = "lights:loadtest:deviceid"

redis_conn_pool = ConnectionPool(host=REDIS_HOST, port=REDIS_PORT)
Redis(connection_pool=redis_conn_pool).set(REDIS_KEY, 0)


def fire_failure(request_type, name, response_time, exception, **kwargs):
    events.request_failure.fire(
        request_type=request_type, name=name, response_time=response_time, exception=exception, **kwargs)


def fire_success(request_type, name, response_time=0, response_length=0, **kwargs):
    events.request_success.fire(
        request_type=request_type, name=name, response_time=response_time, response_length=response_length, **kwargs)


class HelloBehaviour(TaskSet):
    def load_next_device(self):
        self.idx = Redis(connection_pool=redis_conn_pool).incr(REDIS_KEY)
        record = dynamodb.Table('LoadTest_Data').get_item(Key={'id': self.idx})
        self.data = json.loads(record['Item']['data'])

    def init(self):
        self.load_next_device()
        self.serial = self.data['productSerial']
        self.HELLO_MESSAGE = '{\"MessageId\":\"3b1ad025-01b6-476d-a1dd-56d60a5f4da0\",\"Body\":\"{\\\"msg\\\":\\\"HELLO\\\",\\\"time\\\":\\\"2018-06-17T06:26:54Z\\\",\\\"version\\\":\\\"0.0.0|99.3.17\\\",\\\"protocol\\\":\\\"1.0.0\\\",\\\"recovery package version\\\":\\\"\\\",\\\"reset-source\\\":\\\"\\\",\\\"serialNumber\\\":\\\"' + \
                             self.serial + '\\\",\\\"topic\\\":\\\"552\/' + self.serial + '\/status\/connection\\\",\\\"mode-reason\\\":\\\"NONE\\\"}\",\"Headers\":{\"Serial\":\"' + \
                             self.serial + '\",\"ProductType\":\"552\",\"TopicName\":\"552\/' + \
                             self.serial + '\/status\/connection\",\"CorrelationId\":\"257df36f-e6ec-e3ab-19bc-43f3dbac9548\",\"ServerTime\":\"2018-07-13T07:50:03.3480000Z\",\"Region\":\"ap-southeast-2\",\"Environment\":\"lt\"}}'

    def on_start(self):
        self.init()

    def end(self):
        raise StopLocust()

    def sendMessage(self):
        print(f'Sending HELLO message to SNS: {self.HELLO_MESSAGE}')
        response = snsClient.publish(
            TargetArn='arn:aws:sns:us-east-2:615271244883:lights-status',
            Message=self.HELLO_MESSAGE
        )

        if (response['ResponseMetadata']['HTTPStatusCode'] == 200):
            fire_success("HelloMessage", "success")
        else:
            fire_failure("HelloMessage", "Failure", 0,
                         "ResponseCode :" + response['ResponseMetadata']['HTTPStatusCode'])

    @task
    def sendHelloMessage(self):
        self.sendMessage()


class UpgradingSoftwareBehaviour(TaskSet):
    def load_next_device(self):
        self.idx = Redis(connection_pool=redis_conn_pool).incr(REDIS_KEY)
        record = dynamodb.Table('LoadTest_Data').get_item(Key={'id': self.idx})
        self.data = json.loads(record['Item']['data'])

    def init(self):
        self.load_next_device()
        self.serial = self.data['productSerial']
        self.UPGRADING_SOFTWARE_MESSAGE = '{\"MessageId\":\"8ffe6f2d-76f5-4f4d-8417-0ba07e274ae1\",\"Body\":\"{\\\"msg\\\":\\\"UPGRADING-SOFTWARE\\\",\\\"time\\\":\\\"2018-06-22T06:26:59.000Z\\\",\\\"state\\\":\\\"acknowledged\\\",\\\"mode-reason\\\":\\\"NONE\\\",\\\"code\\\":\\\"\\\"}\",\"Headers\":{\"Serial\":\"' + \
                                          self.serial + '\",\"ProductType\":\"552\",\"TopicName\":\"552\/' + \
                                          self.serial + '\/status\/software\",\"CorrelationId\":\"3dd1adad-f9f4-c31f-662b-74f26d613f71\",\"ServerTime\":\"2018-07-13T07:49:24.7990000Z\",\"Region\":\"ap-southeast-2\",\"Environment\":\"lt\"}}'

    def on_start(self):
        self.init()

    def end(self):
        raise StopLocust()

    def sendMessage(self):
        print(f'Sending UPGRADING-SOFTWARE message to SNS: {self.UPGRADING_SOFTWARE_MESSAGE}')
        response = snsClient.publish(
            TargetArn='arn:aws:sns:us-east-2:615271244883:lights-status',
            Message=self.UPGRADING_SOFTWARE_MESSAGE)

        if (response['ResponseMetadata']['HTTPStatusCode'] == 200):
            fire_success("UpragingSoftwareMessage", "success")
        else:
            fire_failure("UpragingSoftwareMessage", "Failure", 0,
                         "ResponseCode :" + response['ResponseMetadata']['HTTPStatusCode'])

    @task
    def sendUpgradingSoftwareMessage(self):
        self.sendMessage()


class HelloMessageSender(Locust):
    task_set = HelloBehaviour
    min_wait = 500
    max_wait = 1000
    weight = 3


class UpgradingSoftwareMessageSender(Locust):
    task_set = UpgradingSoftwareBehaviour
    min_wait = 500
    max_wait = 1000
    weight = 1
